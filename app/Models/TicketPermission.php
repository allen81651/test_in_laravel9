<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 工單編輯權限設定表
 *
 * @property int $id
 * @property string $role 身分
 * @property string $type 工單類型
 * @property string $create 新增
 * @property string $change_status 狀態修改
 * @property string $delete 刪除
 * @property string $created_at 創建時間
 * @property string $updated_at 修改時間
 */
class TicketPermission extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class, 'role', 'role');
    }
}
