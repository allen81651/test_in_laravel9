<?php

namespace Database\Seeders;

use App\Constants\AppConstants;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table((new User())->getTable())->truncate();

        $data = [
            // admin
            [
                'name' => 'admin',
                'email' => null,
                'password' => Hash::make('admin'),
                'role' => AppConstants::ROLE_ADMIN,
            ],
            // QA
            [
                'name' => 'allenQA',
                'email' => null,
                'password' => Hash::make('allen'),
                'role' => AppConstants::ROLE_QA,
            ],
            // RD
            [
                'name' => 'allenRD',
                'email' => null,
                'password' => Hash::make('allen'),
                'role' => AppConstants::ROLE_RD,
            ],
            // PM
            [
                'name' => 'allenPM',
                'email' => null,
                'password' => Hash::make('allen'),
                'role' => AppConstants::ROLE_PM,
            ],
        ];

        User::insert($data);
    }
}
