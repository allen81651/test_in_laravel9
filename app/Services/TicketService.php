<?php

namespace App\Services;

use App\Models\Ticket;
use App\Models\User;
use App\Models\TicketPermission;

/**
 * 工攤服務
 */
class TicketService
{
    public function __construct(
        protected Ticket $ticketModel,
        protected TicketPermission $ticketPermissionModel
    ) {}

    /**
     * 權限判斷
     * 
     * @param string $method 要執行的動作
     * @param User $user 操作者
     * @param array $params 輸入參數
     * @return bool
     */
    public function checkPermissions(string $method, User $user, array $params = [])
    {
        if ($user->isAdmin()) {
            return true;
        }

        switch ($method) {
            case 'create':
                $permission = $user->ticketPermission()->where(['type' => $params['type']])->first();
                if ($permission->{$method}) {
                    return true;
                }
                break;
            case 'update':
                $permissions = $user->ticketPermission->keyBy('type');
                if ($permissions[$params['type']]->create) { // 來源類型
                    $model = $this->detail($params['id']); // 目標類型
                    if ($permissions[$model['type']]->create) {
                        return true;
                    }
                }
                break;
            case 'change_status':
            case 'delete':
                $model = $this->detail($params['id']);
                if (!$model) {
                    return false;
                }
                $permission = $user->ticketPermission()->where(['type' => $model['type']])->first();
                if ($permission->{$method}) {
                    return true;
                }
                break;
        }

        return false;
    }

    /**
     * 工單列表
     *
     * @param array $params 輸入參數
     * @param bool $isAdmin 是否為管理員
     * @return array
     */
    public function list(array $params, bool $isAdmin = false)
    {
        $query = $this->ticketModel
            ->select(['id', 'type', 'summary', 'description', 'status', 'severity', 'priority',])
            ->orderBy('created_at', 'DESC');
        if ($isAdmin) {
            $query->withTrashed();
        }

        $pageSet = [
            'page' => $params['page'] ?? 1,
            'perPage' => $params['per-page'] ?? 20,
        ];
        return $query->simplePaginate(...$pageSet)->toArray();
    }

    /**
     * 工單詳情
     *
     * @param array $id 目標工單ID
     * @param bool $withTrashed 是否包含已刪除工單
     * @return array|null
     */
    public function detail(int $id, $withTrashed = true)
    {
        $query = $this->ticketModel->where(['id' => $id]);
        if ($withTrashed) {
            $query->withTrashed();
        }
        $model = $query->first();

        return $model ? $model->toArray() : null;
    }

    /**
     * 工單創建/編輯
     *
     * @param array $params 輸入參數
     * @param int $userId 操作者ID
     * @return bool
     */
    public function edit(array $params, $userId)
    {
        if (empty($params['id'])) {
            $model = new $this->ticketModel();
            $model->owner_id = $userId;
        } else {
            $model = $this->ticketModel
                ->where(['id' => $params['id']])
                ->first();
        }

        $model->type = $params['type'];
        $model->summary = $params['summary'];
        $model->description = $params['description'];
        $model->status = $params['status'];
        $model->severity = $params['severity'];
        $model->priority = $params['priority'];

        return $model->save();
    }

    /**
     * 工單刪除
     *
     * @param array $id 目標工單ID
     * @return bool
     */
    public function delete(int $id)
    {
        return $this->ticketModel->where(['id' => $id])->delete();
    }

    /**
     * 工單狀態變更
     *
     * @param array $params 輸入參數
     * @return bool
     */
    public function changeStatus(array $params)
    {
        $model = $this->ticketModel
            ->where(['id' => $params['id']])
            ->first();

        $model->status = $params['status'];

        return $model->save();
    }
}
