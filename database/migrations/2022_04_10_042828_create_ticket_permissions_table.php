<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_permissions', function (Blueprint $table) {
            $table->id();
            $table->string('role', 10)->comment('身分')->index();
            $table->string('type', 10)->comment('工單類型');
            $table->tinyInteger('create')->comment('新增');
            $table->tinyInteger('change_status')->comment('狀態修改');
            $table->tinyInteger('delete')->comment('刪除');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `tickets` comment '工單編輯權限設定表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_permissions');
    }
};
