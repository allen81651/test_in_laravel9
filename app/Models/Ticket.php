<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 工單資料表
 *
 * @property int $id
 * @property int $owner_id 創建者ID
 * @property string $type 工單類型
 * @property string $summary 摘要
 * @property string $description 描述
 * @property string $status 狀態
 * @property string $severity 嚴重度
 * @property string $priority 優先級
 * @property string $deleted_at 刪除時間
 * @property string $created_at 創建時間
 * @property string $updated_at 修改時間
 */
class Ticket extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
