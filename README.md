# 練習用專案
- php 8.1
- laravel 9.7
- OpenApi 3.0
- mySql

## OpenApi文件
- yaml格式
- 使用Redoc套件
- url: /redoc-static.html
- 打包好的文檔路徑: ./public/redoc-static.html
- 文檔路徑: ./resources/spec/swagger.yaml
###### redoc-cli安裝:
```bash
yarn global add redoc-cli
```
###### 打包指令: (見package.json)
```bash
yarn spec-build
```