<?php

namespace App\Http\Requests;

use App\Constants\TicketConstants;
use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class TicketChangeStatusRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'integer', 'exists:tickets,id,deleted_at,NULL'], // URL parameters
            'status' => ['required', Rule::in([TicketConstants::STATUS_OPEN, TicketConstants::STATUS_FINISH])],
        ];
    }

    /**
     * Get all of the input and files for the request.
     *
     * {@inheritDoc}
     * @param  array|mixed|null  $keys
     * @return array
     */
    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['id'] = $this->route('id'); // URL parameters

        return $data;
    }
}
