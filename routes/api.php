<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\TicketController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// 登入頁
//      登入API
Route::post('login', [LoginController::class, 'login']);

Route::middleware(['auth:sanctum'])->group(function () {
    // 工單列表
    Route::controller(TicketController::class)->group(function () {
        Route::get('/ticket', 'list'); // 列表
        Route::post('/ticket', 'edit'); // 創建
        Route::get('/ticket/{id}', 'detail'); // 詳情
        Route::put('/ticket/{id}', 'edit'); // 編輯
        Route::put('/ticket/{id}/status', 'changeStatus'); // 狀態變更
        Route::delete('/ticket/{id}', 'delete'); // 刪除
    });
});

// 人員列表
//      列表
//      新增/編輯/刪除帳號
