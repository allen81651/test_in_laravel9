<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketChangeStatusRequest;
use App\Http\Requests\TicketEditRequest;
use App\Http\Requests\TicketListRequest;
use App\Http\Requests\TicketRequest;
use App\Services\TicketService;
use Illuminate\Support\Facades\Auth;

/**
 * 工單相關
 */
class TicketController extends Controller
{
    /**
     * 工單列表API
     *
     * @param TicketListRequest $request
     * @param TicketService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(TicketListRequest $request, TicketService $service)
    {
        $isAdmin = Auth::user()->isAdmin();
        $params = $request->validated();
        $result = $service->list($params, $isAdmin);

        return $this->success($result);
    }

    /**
     * 工單詳情API
     *
     * @param TicketRequest $request
     * @param TicketService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail(TicketRequest $request, TicketService $service)
    {
        $id = $request->validated('id');
        $result = $service->detail($id);

        return $this->success($result);
    }

    /**
     * 工單創建/編輯API
     *
     * @param TicketEditRequest $request
     * @param TicketService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(TicketEditRequest $request, TicketService $service)
    {
        $params = $request->validated();
        $user = Auth::user();
        $method = empty($params['id']) ? 'create' : 'update';
        $checkPermissions = $service->checkPermissions($method, $user, $params);
        if(!$checkPermissions) {
            return $this->forbidden();
        }

        $result = $service->edit($params, $user->id);

        if (!$result) {
            return $this->error('編輯失敗');
        }
        return $this->success();

    }

    /**
     * 工單刪除API
     *
     * @param TicketRequest $request
     * @param TicketService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(TicketRequest $request, TicketService $service)
    {
        $id = $request->validated('id');
        $checkPermissions = $service->checkPermissions('delete', Auth::user(), ['id' => $id]);
        if(!$checkPermissions) {
            return $this->forbidden();
        }

        $result = $service->delete($id);

        if (!$result) {
            return $this->error('刪除失敗');
        }
        return $this->success();
    }

    /**
     * 工單狀態變更API
     *
     * @param TicketChangeStatusRequest $request
     * @param TicketService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(TicketChangeStatusRequest $request, TicketService $service)
    {
        $params = $request->validated();
        $checkPermissions = $service->checkPermissions('change_status', Auth::user(), $params);
        if(!$checkPermissions) {
            return $this->forbidden();
        }

        $result = $service->changeStatus($params);

        if (!$result) {
            return $this->error('狀態變更失敗');
        }
        return $this->success();
    }
}
