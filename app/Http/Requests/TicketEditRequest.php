<?php

namespace App\Http\Requests;

use App\Constants\TicketConstants;
use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class TicketEditRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $severityList = [
            TicketConstants::SEVERITY_TOP,
            TicketConstants::SEVERITY_HIGH,
            TicketConstants::SEVERITY_MIDDLE,
            TicketConstants::SEVERITY_LOW,
            TicketConstants::SEVERITY_LOWEST,
        ];
        $priorityList = [
            TicketConstants::PRIORITY_TOP,
            TicketConstants::PRIORITY_HIGH,
            TicketConstants::PRIORITY_MIDDLE,
            TicketConstants::PRIORITY_LOW,
            TicketConstants::PRIORITY_LOWEST,
        ];
        return [
            'id' => ['nullable', 'integer', 'exists:tickets,id,deleted_at,NULL'], // create OR URL parameters
            'type' => ['required', Rule::in([TicketConstants::TYPE_ERROR, TicketConstants::TYPE_Feature, TicketConstants::TYPE_TEST_CASE])],
            'summary' => ['required', 'string', 'max:50'],
            'description' => ['required', 'string', 'max:1000'],
            'status' => ['required', Rule::in([TicketConstants::STATUS_OPEN, TicketConstants::STATUS_FINISH])],
            'severity' => ['required', Rule::in($severityList)],
            'priority' => ['required', Rule::in($priorityList)],
        ];
    }

    /**
     * Get all of the input and files for the request.
     *
     * {@inheritDoc}
     * @param  array|mixed|null  $keys
     * @return array
     */
    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['id'] = $this->route('id'); // URL parameters

        return $data;
    }
}
