<?php

namespace App\Constants;

/**
 * 工單常數
 */
class TicketConstants
{

    /** @var string 工單類型: 錯誤 */
    const TYPE_ERROR = 'error';
    /** @var string 工單類型: 需求 */
    const TYPE_Feature = 'feature';
    /** @var string 工單類型: 測試案例 */
    const TYPE_TEST_CASE = 'test_case';

    /** @var string 工單狀態: 開啟 */
    const STATUS_OPEN = 'open';
    /** @var string 工單狀態: 完成 */
    const STATUS_FINISH = 'finish';

    /** @var string 工單嚴重度: 最高 */
    const SEVERITY_TOP = 'top';
    /** @var string 工單嚴重度: 高 */
    const SEVERITY_HIGH = 'high';
    /** @var string 工單嚴重度: 中 */
    const SEVERITY_MIDDLE = 'middle';
    /** @var string 工單嚴重度: 低 */
    const SEVERITY_LOW = 'low';
    /** @var string 工單嚴重度: 最低 */
    const SEVERITY_LOWEST = 'lowest';

    /** @var string 工單優先級: 最高 */
    const PRIORITY_TOP = 'top';
    /** @var string 工單優先級: 高 */
    const PRIORITY_HIGH = 'high';
    /** @var string 工單優先級: 中 */
    const PRIORITY_MIDDLE = 'middle';
    /** @var string 工單優先級: 低 */
    const PRIORITY_LOW = 'low';
    /** @var string 工單優先級: 最低 */
    const PRIORITY_LOWEST = 'lowest';
}
