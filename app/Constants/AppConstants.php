<?php

namespace App\Constants;

/**
 * 系統常數
 */
class AppConstants
{
    /** @var int 成功 */
    const SUCCESS = 200;
    /** @var int 錯誤 */
    const ERROR = 400;
    /** @var int 權限不足 */
    const Forbidden = 403;
    /** @var int 驗證失敗 */
    const FALL_VALIDATION = 422;

    /** @var string 帳號身分: 管理員 */
    const ROLE_ADMIN = 'admin';
    /** @var string 帳號身分: 測試 */
    const ROLE_QA = 'qa';
    /** @var string 帳號身分: 開發 */
    const ROLE_RD = 'rd';
    /** @var string 帳號身分: 產品經理 */
    const ROLE_PM = 'pm';
}
