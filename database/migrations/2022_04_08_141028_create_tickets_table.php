<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('owner_id')->comment('創建者ID');
            $table->string('type', 10)->comment('工單類型');
            $table->string('summary', 50)->comment('摘要');
            $table->string('description', 1000)->comment('描述');
            $table->string('status', 10)->comment('狀態');
            $table->string('severity', 10)->comment('嚴重度');
            $table->string('priority', 10)->comment('優先級');
            $table->softDeletes();
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `tickets` comment '工單資料表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
};
