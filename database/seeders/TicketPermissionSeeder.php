<?php

namespace Database\Seeders;

use App\Constants\AppConstants;
use App\Constants\TicketConstants;
use App\Models\TicketPermission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table((new TicketPermission())->getTable())->truncate();

        $data = [
            // admin
            [
                'role' => AppConstants::ROLE_ADMIN,
                'type' => TicketConstants::TYPE_ERROR,
                'create' => 1,
                'change_status' => 1,
                'delete' => 1,
            ],
            [
                'role' => AppConstants::ROLE_ADMIN,
                'type' => TicketConstants::TYPE_Feature,
                'create' => 1,
                'change_status' => 1,
                'delete' => 1,
            ],
            [
                'role' => AppConstants::ROLE_ADMIN,
                'type' => TicketConstants::TYPE_TEST_CASE,
                'create' => 1,
                'change_status' => 1,
                'delete' => 1,
            ],
            // QA
            [
                'role' => AppConstants::ROLE_QA,
                'type' => TicketConstants::TYPE_ERROR,
                'create' => 1,
                'change_status' => 0,
                'delete' => 1,
            ],
            [
                'role' => AppConstants::ROLE_QA,
                'type' => TicketConstants::TYPE_Feature,
                'create' => 0,
                'change_status' => 0,
                'delete' => 0,
            ],
            [
                'role' => AppConstants::ROLE_QA,
                'type' => TicketConstants::TYPE_TEST_CASE,
                'create' => 1,
                'change_status' => 1,
                'delete' => 1,
            ],
            // RD
            [
                'role' => AppConstants::ROLE_RD,
                'type' => TicketConstants::TYPE_ERROR,
                'create' => 0,
                'change_status' => 1,
                'delete' => 0,
            ],
            [
                'role' => AppConstants::ROLE_RD,
                'type' => TicketConstants::TYPE_Feature,
                'create' => 0,
                'change_status' => 1,
                'delete' => 0,
            ],
            [
                'role' => AppConstants::ROLE_RD,
                'type' => TicketConstants::TYPE_TEST_CASE,
                'create' => 0,
                'change_status' => 0,
                'delete' => 0,
            ],
            // PM
            [
                'role' => AppConstants::ROLE_PM,
                'type' => TicketConstants::TYPE_ERROR,
                'create' => 0,
                'change_status' => 0,
                'delete' => 0,
            ],
            [
                'role' => AppConstants::ROLE_PM,
                'type' => TicketConstants::TYPE_Feature,
                'create' => 1,
                'change_status' => 0,
                'delete' => 1,
            ],
            [
                'role' => AppConstants::ROLE_PM,
                'type' => TicketConstants::TYPE_TEST_CASE,
                'create' => 0,
                'change_status' => 0,
                'delete' => 0,
            ],
        ];

        TicketPermission::insert($data);
    }
}
