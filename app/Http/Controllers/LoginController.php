<?php

namespace App\Http\Controllers;

use App\Constants\AppConstants;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * 登入/登出
 */
class LoginController extends Controller
{
    /**
     * 登入API
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $params = $request->validated();

        if (Auth::attempt($params)) {
            // $request->session()->regenerate();
            $token = Auth::user()->createToken('token');

            return $this->success(['token' => $token->plainTextToken]);
        }

        return $this->response(AppConstants::FALL_VALIDATION, '帳密錯誤', []);
    }

    // 登出API
    public function logout(Request $request)
    {
        // todo
    }
}
