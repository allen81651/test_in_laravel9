<?php

namespace App\Http\Controllers;

use App\Constants\AppConstants;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * 制式化回應
     *
     * @param int $subStatus 子狀態
     * @param string $message 訊息
     * @param array $data 回傳資料
     * @param int $status 狀態
     * @return \Illuminate\Http\JsonResponse
     */
    public function response(int $subStatus, string $message, array $data, int $status = AppConstants::SUCCESS)
    {
        return response()->json([
            'status' => $subStatus,
            'message' => $message,
            'data' => $data,
        ], $status);
    }

    /**
     * 成功回傳
     *
     * @param array $data 回傳資料
     * @return \Illuminate\Http\JsonResponse
     */
    public function success(array $data = [])
    {
        return $this->response(AppConstants::SUCCESS, '成功', $data);
    }

    /**
     * 失敗回傳
     *
     * @param string $message 訊息
     * @param array $data 回傳資料
     * @param int $subStatus 子狀態
     * @return \Illuminate\Http\JsonResponse
     */
    public function error(string $message = '錯誤', array $data = [], $subStatus = AppConstants::ERROR)
    {
        return $this->response($subStatus, $message, $data, AppConstants::ERROR);
    }

    /**
     * 權限不足
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function forbidden()
    {
        return response('權限不足', AppConstants::Forbidden);
    }
}
